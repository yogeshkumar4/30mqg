import tensorflow as tf
from utils import *
from QG_GRUCell import QG_GRUCell
from QG_Helper import QG_Helper
from tensorflow.contrib import learn
import random, os, nltk, sys, codecs
from tensorflow.python.layers.core import Dense
import numpy as np
from PyRouge.pyrouge import Rouge

reload(sys)
sys.setdefaultencoding('utf-8')
np.set_printoptions(threshold=np.nan)
os.environ["CUDA_VISIBLE_DEVICES"]='1'

learning_rate = 0.00025
vocab_size = 30000
emb = 200
# hidden_units = 64
gru_num_cells = 600
numLayers = 1
epochs = 1000
batch_size = 32
patience = 10
validation_set = 20
model = "qg_gru"
input_file = "./data/QuestionsComplete.txt"
filename = 'glove.6B.200d.txt'


#Gives the next batc for training/testing
def nextBatch(j, input_l, target_l, lens_l):
    c = list(zip(input_l[j*batch_size:(j+1)*batch_size], target_l[j*batch_size:(j+1)*batch_size], lens_l[j*batch_size:(j+1)*batch_size]))
    random.shuffle(c)
    return zip(*c)

#Function to convert predictions to english sentences
def predToSentences(pred, vocab):  
    sents = []
    for one_pred in pred:
        tokenized_sent = []
        for elem in one_pred:
            #elem == 2 means endtoken symbol is produced
            if elem == vocab.index("endtoken"):
                break
            elif elem == 0:
                tokenized_sent.append('unk')   
                continue 
            tokenized_sent.append(vocab[elem])
        sents.append(' '.join(tokenized_sent)) 
    return sents    

#Function to read from GloVe pretrained word embeddings. Takes a word only if it is present in vocab_t
def loadGloVe(filename, vocab_t):
    vocab = []
    embd = []
    file = open(filename,'r')
    for line in file.readlines():
        row = line.strip().split(' ')
        if row[0] in vocab_t:
            vocab.append(row[0])
            embd.append(row[1:])
    print('Loaded GloVe!')
    file.close()
    return vocab,embd

#Code Snippet to read the Questions file(Target Questions)
with codecs.open(input_file, 'r', encoding='utf-8') as f:
    reader = csv.reader(f, skipinitialspace=True)
    reader.next()
    sentences = reader
    sentences_t = [nltk.word_tokenize(x[0].lower().rstrip()) + ['endtoken'] for x in reader]
    #sentences = [' '.join(x) for x in sentences_t]    
#Calculating the word frequency    
word_freq = nltk.FreqDist(itertools.chain(*sentences_t))
vocab_t = sorted(word_freq.items(), key=lambda x: (x[1], x[0]), reverse=True)[:6998]
vocab_t = [elem[0] for elem in vocab_t]
vocab, embd = loadGloVe(filename, vocab_t)
embd_size = len(embd[0])
#Adding the unknown, starttoken and endtoken with their embeddings(decided at random)
# vocab = ['starttoken', 'endtoken'] + vocab
# embd = [[0]*embd_size, [1]*embd_size, [-1]*embd_size] + embd
vocab_size = len(vocab)
vocab = ['unk', 'starttoken'] + list(set(vocab_t) - set(vocab)) + vocab
print("Vocabulary size of Questions:{0}".format(len(vocab_t)))
print("Words Not found in GloVe:{0}".format(len(vocab_t) - vocab_size))
print("Word Embedding Size:{0}".format(embd_size))
print("Total Number of Training Examples:{0}".format(len(sentences_t)))
embedding = np.asarray(embd)
max_document_length = 20



# vocab_processor = learn.preprocessing.VocabularyProcessor(max_document_length)
# pretrain = vocab_processor.fit(vocab)

#Sentences to word indices. Feed as input for embedding lookup
y_gold = []
for elem in sentences_t:
    temp = []
    for i in range(max_document_length):
        if i >= len(elem):
            temp.append(vocab.index("endtoken"))
        else:
            try:
                temp.append(vocab.index(elem[i]))
            except:
                temp.append(0)
    y_gold.append(temp)                   

#Calling this function to obtain F_atom for each fact
entity2id, id2entity, relation2id, id2relation, entitiesEmbed, relationsEmbed, triples = load_data(input_file, vocab_size)



# y_gold = np.array(list(vocab_processor.transform(sentences)))
#This line encodes each fact into it's embedding
x_train = [encode(elem, entity2id, id2entity, relation2id, id2relation, entitiesEmbed, relationsEmbed) for elem in triples]
lens = [min(len(elem),max_document_length) for elem in sentences_t]
numBatches = len(y_gold)/batch_size
x_train = x_train[:numBatches*batch_size]
y_gold = y_gold[:numBatches*batch_size]
# y_gold = [[elem - 1 if elem > 0 else 0 for elem in x] for x in y_gold]
lens = lens[:numBatches*batch_size]

graph = tf.Graph()
with graph.as_default():
    #-------------------------------------------------------------------------#
    #Snippet for loading pretrained word embedding
    W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embd_size]),trainable=False, name="W")
    embedding_placeholder = tf.placeholder(tf.float32, [vocab_size, embd_size])
    embedding_init = W.assign(embedding_placeholder)

    train_embeddings = tf.get_variable(name="embs_only_in_train", shape=[len(vocab_t) - vocab_size + 2, embd_size], initializer=tf.random_uniform_initializer(-0.04, 0.04),trainable=True)
    embeddings = tf.concat([train_embeddings, W], axis=0)

    #-------------------------------------------------------------------------#
    #input_layer = Dense(hidden_units, dtype=tf.float32, name='input_projection')
    #output_layer = Dense(vocab_size, use_bias=False, name="output_projection")
    #Global step to keep the count of number of updates by loss function
    global_step = tf.Variable(0, trainable=False)

    #-------------------------------------------------------------------------#
    #Placeholders to take the input data
    #decoder_inputs is the list of indices representing sentences
    #decoder_f_atom_s are the fact embedding
    #target_sequence_length are the length of each individual target question
    decoder_inputs = tf.placeholder(dtype=tf.int32, shape=(batch_size, max_document_length))
    decoder_f_atom_s = tf.placeholder(dtype=tf.float32, shape=(batch_size, 3*emb))
    target_sequence_length = tf.placeholder(dtype=tf.int32, shape=(batch_size,))
    #-------------------------------------------------------------------------#
    #decoder_f_atom = tf.expand_dims(decoder_f_atom_s, 1)
    #decoder_f_atom = tf.tile(decoder_f_atom, [1,max_document_length + 1,1])

    #Start token being added
    decoder_start_token = tf.ones(shape=[batch_size, 1], dtype=tf.int32)*1
    # decoder_end_token = tf.ones(shape=[batch_size, 1], dtype=tf.int32)*2 

    #Defining the GRU Cell for decoder
    cell = QG_GRUCell(gru_num_cells, emb, embd_size, len(vocab_t) + 2)
    decoder_cell = tf.contrib.rnn.MultiRNNCell([cell for _ in range(numLayers)])
    decoder_inputs_train = tf.concat([decoder_start_token, decoder_inputs], axis=1)

    #normed_embedding to calculate the cosine distance
    # normed_embedding = tf.nn.l2_normalize(W, dim=1)
    decoder_inputs_embedded = tf.nn.embedding_lookup(embeddings, decoder_inputs_train)
    #decoder_inputs_embedded = input_layer(decoder_inputs_embedded)

    # decoder_inputs_length_train = target_sequence_length + 1 

    #final_decoder_input = tf.concat([decoder_inputs_embedded, decoder_f_atom], 2)

    #helper = tf.contrib.seq2seq.TrainingHelper(final_decoder_input, target_sequence_length, time_major=False)

    #isTest is passes to helper which decides whether to pass ground truth embedding or last predicted encoding to next step of the decoder
    isTest = tf.placeholder(tf.bool)
    
    #-------------------------------------------------------------------------#
    #Variable to learn W_enc
    W_enc = tf.get_variable('W_enc',[emb, gru_num_cells], initializer=tf.contrib.layers.xavier_initializer())

    decoder_f_atom_individual = tf.reshape(decoder_f_atom_s, [batch_size*3, emb])
    modified_input = tf.matmul(decoder_f_atom_individual, W_enc)

    decoder_f_atom_w_enc = tf.reshape(modified_input, [batch_size, 3*gru_num_cells])

    #-------------------------------------------------------------------------#
    #A small neural network to calculate the initial state of the decoder(takes as input the fact embeddings)
    # with tf.variable_scope('Init_State'):
    # init_state_W = tf.get_variable('init_state_W',[3*emb, init_state_cells], initializer=tf.contrib.layers.xavier_initializer())
    # init_state_b = tf.get_variable('init_state_b',[init_state_cells], initializer=tf.constant_initializer(0.0))
    init_proj_W = tf.get_variable('init_proj_W',[3*gru_num_cells, gru_num_cells], initializer=tf.contrib.layers.xavier_initializer())
    init_proj_b = tf.get_variable('init_proj_b',[gru_num_cells], initializer=tf.constant_initializer(0.0))

    decoder_initial_state = tf.nn.tanh(tf.matmul(decoder_f_atom_w_enc, init_proj_W) + init_proj_b)
    #-------------------------------------------------------------------------#
    decoder_initial_state = tuple([decoder_initial_state]*numLayers)

    helper = QG_Helper(decoder_inputs_embedded, decoder_f_atom_w_enc, embeddings, target_sequence_length, isTest, vocab.index('endtoken'), len(vocab_t) + 2)

    #-------------------------------------------------------------------------#
    #Defining and Running the decoder with its inputs
    my_decoder = tf.contrib.seq2seq.BasicDecoder(decoder_cell, helper, decoder_initial_state, output_layer=None)

    max_decoder_length = tf.reduce_max(target_sequence_length)

    (decoder_outputs_train, decoder_last_state_train, decoder_outputs_length_train) = (tf.contrib.seq2seq.dynamic_decode( decoder=my_decoder, output_time_major=False, impute_finished=True, maximum_iterations=max_decoder_length))
    #-------------------------------------------------------------------------#

    # output_minus_f_atom, last_emb, wrd_emb = tf.split(decoder_outputs_train.rnn_output, [len(vocab_t) + 2, embd_size, 3*emb], axis=2)

    decoder_logits_train = tf.identity(decoder_outputs_train.rnn_output)
    #-------------------------------------------------------------------------#
    #Normalising decoder outputs to calculate cosine distance
    # normed_decoder_outputs = tf.nn.l2_normalize(output_minus_f_atom, dim=2)
    # reshape_normed_output = tf.reshape(normed_decoder_outputs, [-1, gru_num_cells])
    # decoder_logits_train_t = tf.matmul(reshape_normed_output, tf.transpose(normed_embedding, [1, 0]))

    # decoder_logits_train = tf.reshape(decoder_logits_train_t, [batch_size, tf.cast(max_decoder_length, tf.int32), vocab_size])
    #-------------------------------------------------------------------------#
    decoder_pred_train = tf.argmax(decoder_logits_train, axis=-1, name='decoder_pred_train')
    # tf.identity(decoder_outputs_train.sample_id[0], name='train_pred')
    # weights = tf.to_float(tf.not_equal(decoder_inputs[:, :-1], 1))

    #Mask to get which outputs correspond to true prediction
    masks = tf.sequence_mask(lengths=target_sequence_length, maxlen=max_decoder_length, dtype=tf.float32, name='masks')

    decoder_inputs_s, _ = tf.split(decoder_inputs, [tf.cast(max_decoder_length, tf.int32), max_document_length - tf.cast(max_decoder_length, tf.int32)], axis=1)

    # decoder_targets_train = tf.concat([decoder_inputs_s, decoder_end_token], axis=1)
    loss = tf.contrib.seq2seq.sequence_loss(logits=decoder_logits_train, targets=decoder_inputs_s, weights=masks, average_across_timesteps=True, average_across_batch=True,)

    # stepwise_cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
    # labels=tf.one_hot(decoder_inputs_s, depth=len(vocab_t) + 2, dtype=tf.float32),
    # logits=decoder_logits_train,)

    # loss = tf.reduce_mean(stepwise_cross_entropy)


    trainable_params = tf.trainable_variables()
    train_names = [v.name for v in trainable_params]
    # learning_rate = tf.train.exponential_decay(starting_learning_rate, global_step, numBatches*2, 0.98, staircase=True)
    opt = tf.train.AdamOptimizer(learning_rate)

    gradients = tf.gradients(loss, trainable_params)
    clip_gradients, _ = tf.clip_by_global_norm(gradients, 0.1)
    updates = opt.apply_gradients(zip(clip_gradients, trainable_params), global_step=global_step)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()


config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
config.gpu_options.allow_growth = True
print("Total Number of Batches:{0}".format(numBatches))
last_loss = 100
last_rouge = 0
finished = False
rouge = Rouge()
with tf.Session(config=config, graph=graph) as sess:
    sess.run(init)
    sess.run(embedding_init, feed_dict={embedding_placeholder: embedding})
    for i in range(epochs):
        if finished:
            break
        training_loss = 0
        sys.stdout.flush()
        for j in range(numBatches - validation_set):
            batchx, batchy, batchlens = nextBatch(j, x_train, y_gold, lens)
            _, t_loss, preds_t = sess.run([updates, loss, decoder_pred_train], feed_dict={decoder_f_atom_s:batchx, decoder_inputs:batchy, target_sequence_length:batchlens, isTest:False})
            training_loss += t_loss/batch_size
        validation_loss = 0  
        rouge_score = 0      
        for j in range(numBatches - validation_set, numBatches):            
            testbatchx, testbatchy, testbatchlen = nextBatch(j, x_train, y_gold, lens)
            preds, v_loss = sess.run([decoder_pred_train, loss], feed_dict={decoder_f_atom_s:testbatchx, decoder_inputs:testbatchy, target_sequence_length:testbatchlen, isTest:True})
            validation_loss += v_loss/batch_size
            sents_p = predToSentences(preds, vocab)
            sents_o = predToSentences(testbatchy, vocab)
            this_score = rouge.rouge_l(sents_p, sents_o)
            rouge_score += this_score[2]
        rouge_score /= validation_set    
        if rouge_score > last_rouge:
            last_rouge = rouge_score
        else:
            if i > 30:
                patience -= 1
                if patience == 0:
                    print("Stopping early as the model has reached the patience limit!!")
                    saver.save(sess, model)
                    finished = True
        print("Epoch:{0}\tTraining Loss:{1}".format(i + 1, training_loss/(numBatches - validation_set)))
        print("\t\t\tValidation Loss:{0}".format(validation_loss/validation_set))
        print("\t\t\tRouge F1 Score:{0}".format(rouge_score))
        # saver.save(sess, model)

    print("Completed Training. Running Final test for last batch!")
    new_saver = tf.train.import_meta_graph(model + '.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('./'))
    testbatchx, testbatchy, testbatchlen = nextBatch(numBatches - 1, x_train, y_gold, lens)
    preds = sess.run([decoder_pred_train], feed_dict={decoder_f_atom_s:testbatchx, decoder_inputs:testbatchy, target_sequence_length:testbatchlen, isTest:True})
    print(predToSentences(preds[0], vocab))
