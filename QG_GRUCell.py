import tensorflow as tf
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import random_ops
from tensorflow.python.ops import math_ops
import numpy as np

np.set_printoptions(threshold=np.nan)

class QG_GRUCell(tf.contrib.rnn.RNNCell):
	""" GRUCell for 30m factoid question generation (https://arxiv.org/pdf/1603.06807.pdf) """

	def __init__(self, num_units, emb_size, word_emb_size, vocab_size):
		"""Initialize the QG_GRU cell.
		Args:
			num_units: int, The number of units in the QG_GRU cell.
		"""

		self.num_units = num_units
		self.activation = tf.nn.tanh
		self.emb_size = emb_size
		self.word_emb_size = word_emb_size
		# self.attn_weights = attn_weights
		self.vocab_size = vocab_size

	@property
	def output_size(self):
		return self.vocab_size

	@property
	def state_size(self):
		return (self.num_units)

	def __call__(self, inputs, state):
		shapes = inputs.get_shape()
		num_inputs = self.word_emb_size
		num_units = self.num_units
		activation = self.activation
		sigmoid = tf.sigmoid
		x, wrd_emb = tf.split(inputs, [self.word_emb_size, 3*num_units], axis=1)
		h = state
		attn_input = tf.concat([wrd_emb, h], axis=1)

		with tf.variable_scope('QG_GRUCell'):
			W_r = tf.get_variable('W_r', [num_inputs, num_units], initializer=tf.contrib.layers.xavier_initializer())
			C_r = tf.get_variable('C_r',[num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			U_r = tf.get_variable('U_r',[num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			# b_r = tf.get_variable('b_r', [num_units], initializer=tf.constant_initializer(0.0))
			W_u = tf.get_variable('W_u', [num_inputs, num_units], initializer=tf.contrib.layers.xavier_initializer())
			C_u = tf.get_variable('C_u',[num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			U_u = tf.get_variable('U_u',[num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			# b_u = tf.get_variable('b_u', [num_units], initializer=tf.constant_initializer(0.0))
			attn_W = tf.get_variable('attn_W',[4*num_units, 3], initializer=tf.contrib.layers.xavier_initializer())
			#Make changes for original
			# attn_b = tf.get_variable('attn_b',[self.attn_weights], initializer=tf.constant_initializer(0.0))
			# attn_proj_W = tf.get_variable('attn_proj_W',[self.attn_weights, 3], initializer=tf.contrib.layers.xavier_initializer())
			# attn_proj_b = tf.get_variable('attn_proj_b',[3], initializer=tf.constant_initializer(0.0))
			W = tf.get_variable('W', [num_inputs, num_units], initializer=tf.contrib.layers.xavier_initializer())
			C = tf.get_variable('C',[num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			U = tf.get_variable('U',[num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			# b = tf.get_variable('b', [num_units], initializer=tf.constant_initializer(0.0))
			V_h = tf.get_variable('V_h', [num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			V_c = tf.get_variable('V_c',[num_units, num_units], initializer=tf.contrib.layers.xavier_initializer())
			V_w = tf.get_variable('V_w',[num_inputs, num_units], initializer=tf.contrib.layers.xavier_initializer())
			# V_b = tf.get_variable('V_b', [num_units], initializer=tf.constant_initializer(0.0))
			V_o = tf.get_variable('V_o',[num_units, self.vocab_size], initializer=tf.contrib.layers.xavier_initializer())


		alphas = tf.nn.softmax(activation(tf.matmul(attn_input,attn_W)))
		# alphas = tf.Print(alphas, [alphas])
		alphas = tf.expand_dims(alphas, 2)
		alphas = tf.tile(alphas, [1, 1, num_units])
		wrd_emb_r = tf.reshape(wrd_emb, [tf.cast(shapes[0], tf.int32), 3, num_units])
		c_prime = wrd_emb_r*alphas
		c = tf.reduce_sum(c_prime, axis=1)

		gr = sigmoid(tf.matmul(x,W_r) + tf.matmul(c, C_r) + tf.matmul(h, U_r))
		gu = sigmoid(tf.matmul(x, W_u) + tf.matmul(c, C_u) + tf.matmul(h, U_u)) 

		h_hat = activation(tf.matmul(x, W) + tf.matmul(c, C) + tf.matmul((gr*h), U))
		h = gu*h + (1-gu)*h_hat
		vocab_softmax = tf.matmul(activation(tf.matmul(x, V_w) + tf.matmul(c, V_c) + tf.matmul(h, V_h)),V_o)
		# ret = tf.concat([vocab_softmax, x, wrd_emb],1)

		return vocab_softmax, (h)
