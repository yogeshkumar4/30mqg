import tensorflow as tf
from utils import *
from QG_GRUCell import QG_GRUCell
from tensorflow.contrib import learn
import random, os, nltk, sys
from tensorflow.python.layers.core import Dense

reload(sys)
sys.setdefaultencoding('utf-8')
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

starting_learning_rate = 0.001
vocab_size = 2000
emb = 200
hidden_units = 512
epochs = 20
batch_size = 32
model = "qg_gru"
input_file = "./data/QuestionsComplete.txt"
filename = 'glove.6B.50d.txt'

def nextBatch(j, input_l, target_l, lens_l):
    c = list(zip(input_l[j*batch_size:(j+1)*batch_size], target_l[j*batch_size:(j+1)*batch_size], lens_l[j*batch_size:(j+1)*batch_size]))
    #random.shuffle(c)
    return zip(*c)

def loadGloVe(filename, vocab_t):
    vocab = []
    embd = []
    file = open(filename,'r')
    for line in file.readlines():
        row = line.strip().split(' ')
        if row[0] in vocab_t:
	        vocab.append(row[0])
	        embd.append(row[1:])
    print('Loaded GloVe!')
    file.close()
    return vocab,embd

with open(input_file, 'rt') as f:
    reader = csv.reader(f, skipinitialspace=True)
    reader.next()
    sentences = reader
    sentences = ["%s" % (x[0].lower()) for x in sentences]    
sentences_t = [nltk.word_tokenize(elem) for elem in sentences]
word_freq = nltk.FreqDist(itertools.chain(*sentences_t))
vocab_t = sorted(word_freq.items(), key=lambda x: (x[1], x[0]), reverse=True)
vocab_t = [elem[0] for elem in vocab_t]
vocab, embd = loadGloVe(filename, vocab_t)
vocab = ['starttoken', 'endtoken'] + vocab
embd = [[0]*len(embd[0]), [1]*len(embd[0])] + embd
vocab_size = len(vocab)
print("Vocabulary size of Questions:{0}".format(len(vocab_t)))
print("Words Not found in GloVe:{0}".format(len(vocab_t) - vocab_size))
print("Total Number of Training Examples:{0}".format(len(sentences)))
embedding_dim = len(embd[0])
embedding = np.asarray(embd)
max_document_length = 20

vocab_processor = learn.preprocessing.VocabularyProcessor(max_document_length)
pretrain = vocab_processor.fit(vocab)

entity2id, id2entity, relation2id, id2relation, entitiesEmbed, relationsEmbed, triples = load_data(input_file, vocab_size)

y_gold = np.array(list(vocab_processor.transform(sentences)))
x_train = [encode(elem, entity2id, id2entity, relation2id, id2relation, entitiesEmbed, relationsEmbed) for elem in triples]
lens = [min(len(elem.split(" ")),max_document_length) for elem in sentences]
numBatches = len(y_gold)/batch_size
x_train = x_train[:numBatches*batch_size]
y_gold = y_gold[:numBatches*batch_size]
lens = lens[:numBatches*batch_size]


W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embedding_dim]),trainable=False, name="W")
embedding_placeholder = tf.placeholder(tf.float32, [vocab_size, embedding_dim])
embedding_init = W.assign(embedding_placeholder)
input_layer = Dense(hidden_units, dtype=tf.float32, name='input_projection')
output_layer = Dense(vocab_size, use_bias=False, name="output_projection")
global_step = tf.Variable(0, trainable=False)

decoder_inputs = tf.placeholder(dtype=tf.int32, shape=(batch_size, max_document_length))
decoder_f_atom_s = tf.placeholder(dtype=tf.float32, shape=(batch_size, 3*emb))
target_sequence_length = tf.placeholder(dtype=tf.int32, shape=(batch_size,))
decoder_f_atom = tf.expand_dims(decoder_f_atom_s, 1)
decoder_f_atom = tf.tile(decoder_f_atom, [1,max_document_length + 1,1])

decoder_start_token = tf.ones(shape=[batch_size, 1], dtype=tf.int32)*1
decoder_end_token = tf.ones(shape=[batch_size, 1], dtype=tf.int32)*2 

decoder_cell = QG_GRUCell(256, emb)
decoder_inputs_train = tf.concat([decoder_start_token, decoder_inputs], axis=1)

decoder_inputs_embedded = tf.nn.embedding_lookup(W, decoder_inputs_train)
decoder_inputs_embedded = input_layer(decoder_inputs_embedded)

decoder_inputs_length_train = target_sequence_length + 1 

final_decoder_input = tf.concat([decoder_inputs_embedded, decoder_f_atom], 2)

helper = tf.contrib.seq2seq.TrainingHelper(final_decoder_input, decoder_inputs_length_train, time_major=False)
decoder_initial_state = tf.zeros([batch_size, 256])
my_decoder = tf.contrib.seq2seq.BasicDecoder(decoder_cell, helper, decoder_initial_state,output_layer=output_layer)

max_decoder_length = tf.reduce_max(decoder_inputs_length_train)

(decoder_outputs_train, decoder_last_state_train, decoder_outputs_length_train) = (tf.contrib.seq2seq.dynamic_decode( decoder=my_decoder, output_time_major=False, impute_finished=True, maximum_iterations=max_decoder_length))

decoder_logits_train = tf.identity(decoder_outputs_train.rnn_output)
decoder_pred_train = tf.argmax(decoder_logits_train, axis=-1, name='decoder_pred_train')
# tf.identity(decoder_outputs_train.sample_id[0], name='train_pred')
# weights = tf.to_float(tf.not_equal(decoder_inputs[:, :-1], 1))

masks = tf.sequence_mask(lengths=decoder_inputs_length_train, maxlen=max_decoder_length, dtype=tf.float32, name='masks')
# print(decoder_logits_train.get_shape())
decoder_inputs_s, _ = tf.split(decoder_inputs, [tf.cast(max_decoder_length, tf.int32) - 1, max_document_length + 1 - tf.cast(max_decoder_length, tf.int32)], axis=1)
decoder_targets_train = tf.concat([decoder_inputs_s, decoder_end_token], axis=1)
loss = tf.contrib.seq2seq.sequence_loss(logits=decoder_logits_train, targets=decoder_targets_train, weights=masks, average_across_timesteps=True, average_across_batch=True,)

trainable_params = tf.trainable_variables()
learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step, numBatches, 0.96, staircase=True)
opt = tf.train.AdamOptimizer(learning_rate)

gradients = tf.gradients(loss, trainable_params)
clip_gradients, _ = tf.clip_by_global_norm(gradients, 5)
updates = opt.apply_gradients(zip(clip_gradients, trainable_params), global_step=global_step)

init = tf.global_variables_initializer()


config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
config.gpu_options.allow_growth = True
print("Total Number of Batches:{0}".format(numBatches))
with tf.Session(config=config) as sess:
    sess.run(init)
    sess.run(embedding_init, feed_dict={embedding_placeholder: embedding})
    for i in range(epochs):
        training_loss = 0
        for j in range(numBatches):
            batchx, batchy, batchlens = nextBatch(j, x_train, y_gold, lens)
            # if j%20 == 0:
            # 	print("Current Batch Number:{0}".format(j + 1))
            # my_mask = sess.run([decoder_outputs_train], feed_dict={decoder_f_atom_s:batchx, decoder_inputs:batchy, target_sequence_length:batchlens})
            # print(my_mask)
            # print(batchy)
            # print(batchlens)
            _, t_loss = sess.run([updates, loss], feed_dict={decoder_f_atom_s:batchx, decoder_inputs:batchy, target_sequence_length:batchlens})
            training_loss += t_loss/batch_size
        print("Epoch:{0}\tTraining Loss:{1}".format(i + 1, training_loss/numBatches))    


