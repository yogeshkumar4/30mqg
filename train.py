#! /usr/bin/env python

import sys
import os
import time
import numpy as np
from utils import *
from datetime import datetime
# import tensorflow as tf

LEARNING_RATE = float(os.environ.get("LEARNING_RATE", "0.00025"))
VOCABULARY_SIZE = int(os.environ.get("VOCABULARY_SIZE", "2000"))
EMBEDDING_DIM = int(os.environ.get("EMBEDDING_DIM", "200"))
HIDDEN_DIM = int(os.environ.get("HIDDEN_DIM", "600"))
NEPOCH = int(os.environ.get("NEPOCH", "20"))
MODEL_OUTPUT_FILE = os.environ.get("MODEL_OUTPUT_FILE")
INPUT_DATA_FILE = os.environ.get("INPUT_DATA_FILE", "./data/Questions.txt")
PRINT_EVERY = int(os.environ.get("PRINT_EVERY", "25000"))

if not MODEL_OUTPUT_FILE:
  ts = datetime.now().strftime("%Y-%m-%d-%H-%M")
  MODEL_OUTPUT_FILE = "GRU-%s-%s-%s-%s.dat" % (ts, VOCABULARY_SIZE, EMBEDDING_DIM, HIDDEN_DIM)

# Load data
x_train, y_train, word_to_index, index_to_word, entity2id, id2entity, relation2id, id2relation, entitiesEmbed, relationsEmbed, triples = load_data(INPUT_DATA_FILE, VOCABULARY_SIZE)

# Build model
# model = GRUTheano(VOCABULARY_SIZE, hidden_dim=HIDDEN_DIM, bptt_truncate=-1)

# Print SGD step time
t1 = time.time()
print(triples[1])
atom = encode(triples[1], entity2id, id2entity, relation2id, id2relation, entitiesEmbed, relationsEmbed)
print("hi")
print(len(atom))
print(len(atom[0]))
print(x_train[1])
print(y_train[1])
print("bye")

# model.sgd_step(x_train[1], np.zeros(200), y_train[1], LEARNING_RATE)
# t2 = time.time()
# print "SGD Step time: %f milliseconds" % ((t2 - t1) * 1000.)
# sys.stdout.flush()

# # We do this every few examples to understand what's going on
# def sgd_callback(model, num_examples_seen):
#   dt = datetime.now().isoformat()
#   loss = model.calculate_loss(x_train[:10000], y_train[:10000])
#   print("\n%s (%d)" % (dt, num_examples_seen))
#   print("--------------------------------------------------")
#   print("Loss: %f" % loss)
#   # generate_sentences(model, 10, index_to_word, word_to_index)
#   save_model_parameters_theano(model, MODEL_OUTPUT_FILE)
#   print("\n")
#   sys.stdout.flush()

# for epoch in range(NEPOCH):
#   train_with_sgd(model, x_train, y_train, entity2id, id2entity, relation2id, id2relation, entitiesEmbed, relationsEmbed, triples, learning_rate=LEARNING_RATE, nepoch=1, decay=0.9, 
#     callback_every=PRINT_EVERY, callback=sgd_callback)

