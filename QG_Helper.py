import tensorflow as tf
from tensorflow.contrib.seq2seq.python.ops import decoder
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.layers import base as layers_base
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import embedding_ops
from tensorflow.python.ops import gen_array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import tensor_array_ops
from tensorflow.python.ops.distributions import bernoulli
from tensorflow.python.ops.distributions import categorical
from tensorflow.python.util import nest


_transpose_batch_time = decoder._transpose_batch_time

def _unstack_ta(inp):
  return tensor_array_ops.TensorArray(
      dtype=inp.dtype, size=array_ops.shape(inp)[0],
      element_shape=inp.get_shape()[1:]).unstack(inp)

class QG_Helper(tf.contrib.seq2seq.Helper):
	""" TestHelper for 30m factoid question generation (https://arxiv.org/pdf/1603.06807.pdf) """

	def __init__(self, inputs, f_atom_input, embedding, sequence_length, isTest, end_token, vocab_size, time_major=False, name=None):
		"""Initializer.
		Args:
		  inputs: A (structure of) input tensors.
		  sequence_length: An int32 vector tensor.
		  time_major: Python bool.  Whether the tensors in `inputs` are time major.
			If `False` (default), they are assumed to be batch major.
		  name: Name scope for any created operations.
		Raises:
		  ValueError: if `sequence_length` is not a 1D tensor.
		"""
		with ops.name_scope(name, "TrainingHelper", [inputs, sequence_length]):
			inputs = ops.convert_to_tensor(inputs, name="inputs")
			if not time_major:
				inputs = nest.map_structure(_transpose_batch_time, inputs)

			self._input_tas = nest.map_structure(_unstack_ta, inputs)
			self._f_atom_input = f_atom_input
			self._vocab_size = vocab_size
			self._embedding_fn = (lambda ids: embedding_ops.embedding_lookup(embedding, ids))
			self._isTest = isTest
			self._end_token = end_token
			self._sequence_length = ops.convert_to_tensor(sequence_length, name="sequence_length")
			if self._sequence_length.get_shape().ndims != 1:
				raise ValueError(
					"Expected sequence_length to be a vector, but received shape: %s" %
					self._sequence_length.get_shape())

			self._zero_inputs = nest.map_structure(lambda inp: array_ops.zeros_like(inp[0, :]), inputs)
			self._batch_size = array_ops.size(sequence_length)



	@property
	def batch_size(self):
		return self._batch_size

	def initialize(self, name=None):
		with ops.name_scope(name, "QG_HelperInitialize"):
			finished = math_ops.equal(0, self._sequence_length)
			all_finished = math_ops.reduce_all(finished)
			input_embedding = self._embedding_fn(1)
			input_embedding = tf.expand_dims(input_embedding, 0)
			input_embedding = tf.tile(input_embedding, [self._batch_size,1])
			next_inputs_concat = tf.concat([input_embedding, self._f_atom_input], 1)
			next_inputs = control_flow_ops.cond(
				all_finished, lambda: tf.concat([self._zero_inputs, self._f_atom_input], 1),
				lambda: next_inputs_concat)
		return (finished, next_inputs)

	def sample(self, time, outputs, name=None, **unused_kwargs):
		with ops.name_scope(name, "QGHelperSample", [time, outputs]):
			# relevant_outputs, x, _ = tf.split(outputs, [self._vocab_size, 200, 600], axis=1)
			sample_ids = math_ops.cast(
			math_ops.argmax(outputs, axis=-1), dtypes.int32)
		return sample_ids	

	def next_inputs(self, time, outputs, state, sample_ids, name=None, **unused_kwargs):
		"""next_inputs_fn for QGHelper."""
		with ops.name_scope(name, "QGHelperNextInputs", [time, outputs, state]):
			next_time = time + 1
			def next_inputs_train():
				finished = (next_time >= self._sequence_length)
				all_finished = math_ops.reduce_all(finished)
				def read_from_ta(inp):
					return inp.read(next_time)
				input_embedding = control_flow_ops.cond(all_finished, lambda: self._zero_inputs, lambda: nest.map_structure(read_from_ta, self._input_tas))
				next_inputs = tf.concat([input_embedding, self._f_atom_input], 1)
				return (finished, next_inputs, state)
			
				
			def next_inputs_test():
				finished = (next_time >= self._sequence_length)
				all_finished = math_ops.reduce_all(finished)	
				input_embedding = self._embedding_fn(sample_ids)
				next_inputs_concat = tf.concat([input_embedding, self._f_atom_input], 1)
				# next_inputs_concat = outputs
				next_inputs = control_flow_ops.cond(
					all_finished, lambda: tf.concat([self._zero_inputs, self._f_atom_input], 1),
					lambda: next_inputs_concat)
				return (finished, next_inputs, state)	
			retValue = tf.cond(self._isTest, next_inputs_test, next_inputs_train)
			return retValue	


